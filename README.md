



# ESMaster

A command-line tool for interacting with ElasticSearch.



## Connection

Default it will connect to `127.0.0.1` with the port `9200`.

If the environemnt variables `ES_HOST` and `ES_PORT` are set, it will use them to build the connection string.

Setting this variables on Windows

    SET ES_HOST=10.0.0.1
    SET ES_PORT=9200

or on Linux

    export ES_HOST=10.0.0.1
    export ES_PORT=9200

And the last method is to give the host and port at running time using `-h` flag for host and `-p` for port

    esmaster -h 10.0.0.10 -p 9999 command command_options



## Commands

Only `GET` requests are supported in this moment

### cat

Sends `cat` commands

    esmaster cat -o indices


### raw

Sends raw requests

    esmaster raw _cat/indices


### data

Request for data using a SQL like syntax

    esmaster  data select * from an_index.a_type where id=1


### describe option


list all types in an index

    esmaster describe an_index


show all mappings for a given combination of index and type

    esmaster describe an_index.a_type




## Limitations

A lot, the product is a very early stage of development
