package main

import (
	"fmt"
	"log"
	"strings"
	"unicode/utf8"
)

// Cat - related operations

func init() {
	// parse args here, if necessary
	var Cat = Command{}
	Cat.Run = runCat
	Cat.Name = "cat"
	Commands["cat"] = &Cat
}

func runCat(cmd *Command, ESurl, version string, args []string) {

	// if len(args) < 1 {
	// 	cmd.printUsage()
	// 	os.Exit(1)
	// }

	var option string
	cmd.Flag.StringVar(&option, "o", "", "the option for CAT")
	cmd.Flag.Parse(args)
	// fmt.Println("usage: ", cmd.Flag.Usage)

	cmd.Name = "_" + cmd.Name
	url := ESurl + "/" + cmd.Name + "/" + option
	// url := ESurl + "/" + cmd.Name + "/" + option + "?help"
	// url := ESurl + "/" + cmd.Name + "/" + option + "?v"
	// url := ESurl + "/" + cmd.Name + "/" + option + "?format=json"
	fmt.Println(url)

	fmt.Println("------------------------HELP---------------------------")
	r := ESReq("GET", url+"?help", []byte(`{}`), version)
	fmt.Println(string(r))

	fmt.Println("------------------------RESULTS------------------------")
	r = ESReq("GET", url+"?v", []byte(`{}`), version)
	// fmt.Println(string(r))

	m := strings.Split(string(r), "\n")
	header := m[0]
	hline := strings.Repeat("-", utf8.RuneCountInString(header))
	fmt.Printf("%s", hline)
	fmt.Printf("\n%s", header)
	fmt.Printf("\n%s", hline)
	for _, r := range m[1:] {
		fmt.Printf("\n%s", r)
	}
	fmt.Printf("%s", hline)

	log.SetFlags(0)
}
