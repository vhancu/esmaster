package main

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"text/template"
)

func containsNOCASE(s []string, e string) int {
	for i, a := range s {
		if strings.ToUpper(a) == strings.ToUpper(e) {
			return i
		}
	}
	return -1
}

func parseColumn(c string) (cn string, cf int) {
	pc := strings.Split(c, ":")
	cn = pc[0]
	cf = 10
	if len(pc) == 1 {
		cf = len(cn)
	} else if len(pc) == 2 {
		format := pc[1]
		if i, err := strconv.Atoi(format); err == nil {
			cf = i
		} else {
			cf = len(cn)
			fmt.Println("error s_to_i:", err)
		}

	}
	return cn, cf
}

func parseColumns(cols []string) (map[string]int, error) {

	// unify all arguments in a string
	a := strings.Join(cols, "")
	// split by comma as column separator
	rcols := strings.Split(a, ",")

	columns := make(map[string]int)

	cn, cf := parseColumn(rcols[0])

	if cn == "*" {
		columns["ALL"] = cf
		return columns, nil
	}
	for _, c := range rcols {
		cn, cf := parseColumn(c)
		columns[cn] = cf

	}

	return columns, nil
}

func ParseDestination(destination []string) (string, string, error) {
	// TODO
	a := strings.Split(destination[0], ".")
	if len(a) == 1 {
		// only index
		return a[0], "", nil
	} else if len(a) == 2 {
		return a[0], a[1], nil
	} else {
		log.Fatalln("INDEX is missing")
	}
	// fmt.Println(a)
	return "", "", errors.New("ParseDestination: some error")
}

func parseFilter(filters []string) ([]string, error) {
	// TODO
	a := strings.Split(filters[0], "=")
	if len(a) == 2 {
		return a, nil
	}
	// fmt.Println(a)
	return nil, errors.New("parseFilter: some error")
}

type SelectStatement struct {
	Operation string
	Column    map[string]int
	Fields    string
	Index     string
	Type      string
	Filter    []string
	From      int
	Size      int
}

func ParseSelect(query []string) (SelectStatement, error) {
	// fmt.Println("[+]Parsing the query...")
	// fmt.Println("      ", query)

	// parsed := make(map[string][]string)
	var parsed SelectStatement

	idxSelect := containsNOCASE(query, "select")
	if idxSelect != 0 {
		return parsed, errors.New("SELECT not in first position")
	}
	parsed.Operation = "SELECT"

	idxFrom := containsNOCASE(query, "from")
	if idxFrom == -1 {
		return parsed, errors.New("Missing FROM the the query")
	}

	d, _ := parseColumns(query[idxSelect+1 : idxFrom])
	parsed.Column = d

	temp := make([]string, len(d))
	i := 0
	for k, _ := range d {
		// fmt.Println(i)
		if k != "ALL" {
			temp[i] = "\"" + k + "\""
			i += 1
		}
	}
	fmt.Println(temp)
	parsed.Fields = strings.Join(temp, ", ")

	idxWhere := containsNOCASE(query, "where")
	// fmt.Println(idxSelect, idxFrom, idxWhere)

	var target []string
	if idxWhere != -1 {
		parsed.Filter, _ = parseFilter(query[idxWhere+1:])
		target = query[idxFrom+1 : idxWhere]
	} else {
		target = query[idxFrom+1:]
	}

	// TODO: map FROM/SIZE to  OFFSET/LIMIT
	idxOffset := containsNOCASE(query, "offset")
	if idxOffset != -1 {
		x := query[idxOffset+1 : idxOffset+2][0]
		fmt.Println("offset:", x)
		if i, err := strconv.Atoi(x); err == nil {
			parsed.From = i
		} else {
			parsed.From = 0 // default
			fmt.Println("error s_to_i:", err)
		}
	}

	idxLimit := containsNOCASE(query, "limit")
	if idxLimit != -1 {
		x := query[idxLimit+1 : idxLimit+2][0]
		fmt.Println("limit", x)
		if i, err := strconv.Atoi(x); err == nil {
			parsed.Size = i
		} else {
			parsed.Size = 10 //default
			fmt.Println("error s_to_i:", err)
		}
	}

	parsed.Index, parsed.Type, _ = ParseDestination(target)

	// fmt.Println("   parsed version:")
	// fmt.Println("      ", parsed)
	return parsed, nil
}

func BuildQuery(parsed SelectStatement) (string, []byte, error) {
	// fmt.Println("[+]Building the query...")
	// fmt.Println("      ", parsed)

	u := ""

	u += parsed.Index

	if parsed.Type != "" {
		u += "/" + parsed.Type
	}

	idxID := containsNOCASE(parsed.Filter, "id")
	if idxID != -1 {
		u += "/" + parsed.Filter[idxID+1]
	}

	u += "/_search"

	const letter = `
	{
		{{if .From}}
		"from" : {{.From}}, 
		{{- end}}
		{{if .Size}}
		"size" : {{.Size}}, 
		{{- end}}
		{{if .Fields}}
		"_source": [{{.Fields}}],
		{{- end}}
		"query": { "match_all": {}
	    } 
	}`
	// Create a new template and parse the letter into it.
	t := template.Must(template.New("letter").Parse(letter))

	var buffer bytes.Buffer
	err := t.Execute(&buffer, parsed)
	if err != nil {
		panic(err)
	}
	post := []byte(buffer.String())

	return u, post, nil
}
