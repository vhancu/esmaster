package main

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"

	"strconv"

	"github.com/stretchr/stew/objects"
)

// Data - search for some data on ES

func init() {
	// parse args here, if necessary
	var Data = Command{}
	Data.Run = runData
	Data.Name = "Data"
	Commands["data"] = &Data
}

type Row struct {
	Index_  string                 `json:"_index"`
	Type_   string                 `json:"_type"`
	Id_     string                 `json:"_id"`
	Score_  float32                `json:"_score"`
	Source_ map[string]interface{} `json:"_source"`
}

type Hits struct {
	Total     int     `json:"total"`
	Max_score float32 `json:"max_score"`
	Hits      []Row   `json:"hits"`
}

type Shards struct {
	Total      int `json:"total"`
	Successful int `json:"successful"`
	Failed     int `json:"failed"`
}

type Result struct {
	Took      int    `json:"took"`
	Timed_out bool   `json:"timed_out"`
	Shards_   Shards `json:"_shards"`
	Hits_     Hits   `json:"hits"`
}

func formatHeader(mappings []string, colFormat map[string]int) string {

	all, ok := colFormat["ALL"]
	res := "|"

	for _, v := range mappings {
		if ok {
			// all columns
			fmt.Println("ALL", all)
			sprintf := "%-" + strconv.Itoa(all) + "s"
			res += " " + fmt.Sprintf(sprintf, v)[:all] + " |"
		} else {
			// project columns
			if format, k := colFormat[v]; k {
				// res += fmt.Sprintf(" %v |", v)
				sprintf := "%-" + strconv.Itoa(format) + "s"
				res += " " + fmt.Sprintf(sprintf, v)[:format] + " |"
			}
		}
	}
	return res
}

func formatRow(row Row, mappings []string, colFormat map[string]int) string {

	all, ok := colFormat["ALL"]

	res := "| "
	for _, v := range mappings {
		if ok {
			// all columns
			sprintf := "%-" + strconv.Itoa(all) + "v"
			temp := fmt.Sprintf(sprintf, row.Source_[v])
			res += temp[:all] + " | "
		} else {
			// project columns
			if format, k := colFormat[v]; k {
				sprintf := "%-" + strconv.Itoa(format) + "v"
				temp := fmt.Sprintf(sprintf, row.Source_[v])
				res += temp[:format] + " | "
			}
		}
	}

	return res
}

func runData(cmd *Command, ESurl, version string, args []string) {

	// fmt.Println(args)
	hline := strings.Repeat("-", 80)
	_ = hline

	// PARSE THE QUERY
	es, err := ParseSelect(args)
	if err != nil {
		fmt.Println("[E] Parsing of the query failed")
		log.Fatal(err)
	}

	fmt.Println(es)

	// GET MAPPINGS
	var mappings []string
	var m map[string]interface{}

	url := ESurl + "/" + es.Index + "/" + es.Type
	fields := ESReq("GET", url+"/_mapping?pretty", []byte(`{}`), version)

	err = json.Unmarshal(fields, &m)
	if err != nil {
		panic(err)
	}

	path := es.Index + ".mappings." + es.Type + ".properties"
	// fmt.Println(path)
	if props, ok := objects.Map(m).Get(path).(map[string]interface{}); ok {
		for k := range props {
			// fmt.Println(k)
			mappings = append(mappings, k)
		}
	}

	fmt.Println(strings.Repeat("=", 80))
	fmt.Println("Mappings :", mappings)

	// BUILD REQUEST
	u, post, _ := BuildQuery(es)

	url = ESurl + "/" + u
	// url += "?pretty"
	fmt.Println("============================> QUERY <============================")
	fmt.Println(url)
	fmt.Println(string(post))

	fmt.Println("===========================> RESULTS <===========================")
	r := ESReq("GET", url+"?pretty", post, version)

	var results Result
	err = json.Unmarshal(r, &results)
	if err != nil {
		panic(err)
	}
	// fmt.Println(string(r))
	// fmt.Println(results)

	// PRINT COLUMNS
	s := formatHeader(mappings, es.Column)
	fmt.Println(strings.Repeat("-", len(s)))
	fmt.Println(s)
	fmt.Println(strings.Repeat("-", len(s)))

	// PRINT DATA
	for _, row := range results.Hits_.Hits {
		s := formatRow(row, mappings, es.Column)
		fmt.Println(s)
	}
	fmt.Println(strings.Repeat("-", len(s)))

	log.SetFlags(0)
}
