package main

import (
	"encoding/json"
	"fmt"
	"log"
	"sort"
	"strings"

	"reflect"
	"strconv"
)

// Describe shows some information about the an index or a type

func init() {
	// parse args here, if necessary
	var Desc = Command{}
	Desc.Run = runDesc
	Desc.Name = "Desc"
	Commands["desc"] = &Desc
}

func printMapping(mapping map[string]string) {

	n := 0
	keys := make([]string, 0)
	for k, _ := range mapping {
		if len(k) > n {
			n = len(k)
		}
		if k != "" {
			keys = append(keys, k)
		}
	}
	sort.Strings(keys)
	fmtString := "      %-" + strconv.Itoa(n+3) + "v :%v\n"
	for _, k := range keys {
		fmt.Printf(fmtString, k, mapping[k])
	}
}

func rec(m map[string]interface{}, lvl int, path, parent string, cols []string) []string {
	for k, v := range m {

		f := reflect.ValueOf(v)
		switch f.Kind() {
		case reflect.Map:
			p := path + "." + k
			cols = rec(v.(map[string]interface{}), lvl+1, p, k, cols)
		case reflect.Slice:
			fallthrough
		case reflect.String:
			// fmt.Printf("%2d%s.%s", lvl, path, k)
			// fmt.Printf(":%s\n", v)
			cols = append(cols, fmt.Sprintf("%s.%s:%s", path, k, v))
			return cols
		default:
			fmt.Printf("%2d%s.%s", lvl, path, k)
			fmt.Printf(":%s\n", v)
			fmt.Printf(":%s\n", f.Kind())
			panic("unhandled case")
		}
	}
	return cols
}

func AllIndeces(vs []string, t string) []int {
	res := make([]int, 0)
	for i, v := range vs {
		if v == t {
			res = append(res, i)
		}
	}
	// res = append(res, -1)
	return res
}

func stage2(col string) (string, string) {

	r := strings.Split(col, ":")
	colName := r[0]
	colType := r[1]

	cns := strings.Split(colName, ".")

	allIndeces := AllIndeces(cns, "properties")
	// fmt.Println(allIndeces)

	path := make([]string, 0)
	for _, c := range allIndeces {
		path = append(path, cns[c+1])
	}

	return strings.Join(path, "."), colType
}

func runDesc(cmd *Command, ESurl, version string, args []string) {

	hline := strings.Repeat("-", 80)
	_ = hline
	index_, type_, err := ParseDestination(args)

	// build query
	m := make(map[string]interface{})

	url := ESurl + "/" + index_
	if type_ != "" {
		url += "/" + type_
	}
	url += "/_mapping?pretty"

	fmt.Println("============================> QUERY <============================")
	fmt.Println(url)

	// request mappings
	fields := ESReq("GET", url, []byte(`{}`), version)

	err = json.Unmarshal(fields, &m)
	if err != nil {
		panic(err)
	}

	cols := rec(m, 1, "", "", make([]string, 1))

	mappings := make(map[string]string)
	for _, col := range cols {
		if col != "" {
			colName, colType := stage2(col)
			mappings[colName] = colType
		}
	}

	fmt.Println("===========================> RESULTS <===========================")
	fmt.Printf("%s\n   %s\n", index_, type_)
	printMapping(mappings)

	log.SetFlags(0)
}
