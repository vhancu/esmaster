package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"runtime"
	"strings"
)

// Command - all info related to a single command
type Command struct {
	Run  func(cmd *Command, ESurl, version string, args []string)
	Flag flag.FlagSet

	Name  string
	Usage string
	Short string
	Long  string

	APIurl  string
	Example string
}

// Commands - map of all supported commands
var Commands = make(map[string]*Command)

func (c *Command) printUsage() {
	fmt.Println(strings.TrimSpace(c.Usage))
}

// ESReq - main function to send commands to the ElasticSearch
func ESReq(method, path string, post []byte, version string) []byte {

	// var jsonStr = post
	postData := bytes.NewBuffer(post)
	// fmt.Println("postDATA:\n", postData)
	req, err := http.NewRequest(method, path, postData)
	if err != nil {
		log.Fatal(err)
	}
	userAgent := "es/" + version + " (" + runtime.GOOS + "-" + runtime.GOARCH + ")"
	req.Header.Set("User-Agent", userAgent)
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	return body
}

func main() {
	log.SetFlags(0)
	const version string = "0.1"

	protocol := "http"

	// default is localhost
	host := "127.0.0.1"
	port := "9200"

	// second in order are ENV variables: ES_HOST and ES_PORT
	if s := os.Getenv("ES_HOST"); s != "" {
		host = s
	}

	if s := os.Getenv("ES_PORT"); s != "" {
		port = s
	}

	// third, read host/port from command line
	hostPtr := flag.String("h", host, "the host of ES instance")
	portPtr := flag.String("p", port, "the port of ES instance")
	flag.Parse()
	host = *hostPtr
	port = *portPtr

	if len(flag.Args()) < 1 {
		fmt.Println("TODO: usage")
		fmt.Println("no input")
		os.Exit(1)
	}

	var command string
	command = flag.Args()[0]

	ESurl := protocol + "://" + host + ":" + port

	cmd, prs := Commands[command]
	if prs {
		fmt.Println("Running...", command)
	} else {
		fmt.Println("[E]unknown command:", command)
		fmt.Println("Exiting...")
		os.Exit(1)
	}

	cmd.Run(cmd, ESurl, version, flag.Args()[1:])

}
