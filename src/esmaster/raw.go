package main

import (
	"fmt"
	"log"
	"strings"
)

// Raw - run a raw commmand on ES

func init() {
	// parse args here, if necessary
	var Raw = Command{}
	Raw.Run = runRaw
	Raw.Name = "raw"
	Commands["raw"] = &Raw
}

func runRaw(cmd *Command, ESurl, version string, args []string) {

	url := ESurl + "/" + strings.Join(args[:], "")
	// url := ESurl + "/" + cmd.Name + "/" + option + "?pretty"
	fmt.Println(url)

	fmt.Println("------------------------RESULTS------------------------")
	r := ESReq("GET", url, []byte(`{}`), version)
	fmt.Println(string(r))

	log.SetFlags(0)
}
